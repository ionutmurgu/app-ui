package com.ionutmurgu.appui;


import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppUiApplication {

    public static void main(String[] args) {

        Application.launch(FXApplication.class, args);
    }
}
